package us.skyguardian.driverns6;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class HolderMensaje extends RecyclerView.ViewHolder {

    private TextView mensaje;
    private TextView fechahora;

    public HolderMensaje(@NonNull View itemView) {
        super(itemView);
        mensaje = (TextView) itemView.findViewById(R.id.xml_lblmsgchat);
        fechahora = (TextView) itemView.findViewById(R.id.xml_lblchathour);
    }

    public TextView getMensaje() {
        return mensaje;
    }

    public void setMensaje(TextView mensaje) {
        this.mensaje = mensaje;
    }

    public TextView getFechahora() {
        return fechahora;
    }

    public void setFechahora(TextView fechahora) {
        this.fechahora = fechahora;
    }
}
