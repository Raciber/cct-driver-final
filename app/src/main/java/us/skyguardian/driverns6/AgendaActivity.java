package us.skyguardian.driverns6;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AgendaActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<List<ListItem>> arraylist;
    MyRecyclerAdapter adapter;
    private ProgressDialog progressDialog;
    ArrayList<ListItem> arrayList = new ArrayList<>();

    String recibeVal;
    String datoLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_agenda);

        recyclerView = (RecyclerView)findViewById(R.id.add_header);

        recibeVal = getIntent().getStringExtra("valida");
        if(recibeVal.equals("1")){
            datoLogin = getIntent().getStringExtra("tellogin");
            //System.out.println(datoLogin);
            int validaInternet = verificarSenal();
            if(validaInternet == 1) {
                new getAgendaDriver().execute(datoLogin, "1");
            }else{
                Toast.makeText(AgendaActivity.this,"Se perdió la conexión a Internet, vuelva a intentar.",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }else{
            datoLogin = getIntent().getStringExtra("keylogin");
            //System.out.println(datoLogin);
            int validaInternet = verificarSenal();
            if(validaInternet == 1) {
                new getAgendaDriver().execute(datoLogin, "2");
            }else{
                Toast.makeText(AgendaActivity.this,"Se perdió la conexión a Internet, vuelva a intentar.",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }



    }

    private class getAgendaDriver extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(AgendaActivity.this);
            progressDialog.setMessage("Cargando agenda...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                if(params[1].equals("1")){
                    paramObject.put("telefono", "+52"+params[0]);
                }else{
                    paramObject.put("llave", params[0]);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/agenda")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            /*progressBar.setVisibility(View.GONE);
            lblloading.setVisibility(View.GONE);*/

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            String fechaHoy = dateFormat.format(date);
            String fechaManana = dateFormat.format(sumarRestarDiasFecha(date, 1));
            String fechaManana1 = dateFormat.format(sumarRestarDiasFecha(date, 2));
            //System.out.println(fechaHoy);
            //System.out.println(fechaManana);
            //System.out.println(fechaManana1);

            //System.out.println(sumarRestarDiasFecha(date, 2));

            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){
                    progressDialog.cancel();
                    for(int i=0; i < jo.getJSONArray("inf").length(); i++){
                        if(fechaHoy.equals(jo.getJSONArray("inf").getJSONArray(0).get(0)) || fechaManana.equals(jo.getJSONArray("inf").getJSONArray(0).get(0)) || fechaManana1.equals(jo.getJSONArray("inf").getJSONArray(0).get(0)) ) {
                            //System.out.println(jo.getJSONArray("inf").getJSONArray(i).get(0));
                            Header header = new Header();
                            header.setHeader("Fecha de viaje: " + jo.getJSONArray("inf").getJSONArray(i).get(0).toString());
                            arrayList.add(header);
                            for (int j = 0; j < jo.getJSONArray("inf").length(); j++) {
                                //System.out.println(jo.getJSONArray("inf").getJSONArray(j).getString(1));
                                ContentItem item = new ContentItem();
                                String auxstr = "Fecha de salida: ";
                                String auxstr2 = "Origen de viaje: ";
                                String auxstr3 = "Destino de viaje: ";

                                SpannableString str = new SpannableString(auxstr + jo.getJSONArray("inf").getJSONArray(j).get(3).toString());
                                str.setSpan(new StyleSpan(Typeface.BOLD), 0, auxstr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                item.setFecha(str);

                                SpannableString str2 = new SpannableString(auxstr2 + jo.getJSONArray("inf").getJSONArray(j).get(1).toString());
                                str2.setSpan(new StyleSpan(Typeface.BOLD), 0, auxstr2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                item.setOrigen(str2);

                                SpannableString str3 = new SpannableString(auxstr3 + jo.getJSONArray("inf").getJSONArray(j).get(2).toString());
                                str3.setSpan(new StyleSpan(Typeface.BOLD), 0, auxstr3.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                item.setDestino(str3);

                                arrayList.add(item);
                            }
                        }else{
                            Header header = new Header();
                            header.setHeader("No hay viajes proximos...");
                            arrayList.add(header);
                        }
                    }

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AgendaActivity.this);
                    adapter = new MyRecyclerAdapter(arrayList);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setAdapter(adapter);

                    //Intent intent = new Intent (LoginActivity.this, actividades.class);
                    //intent.putExtra("param", result);
                    //startActivity(intent);
                }else{
                    progressDialog.cancel();
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Error en número celular o llave móvil, verifique e intentelo de nuevo.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }



            //System.out.println(.*)

        }
    }

    public Date sumarRestarDiasFecha(Date fecha, int dias){

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(fecha); // Configuramos la fecha que se recibe

        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0

        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

    }

    private int verificarSenal(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

}
