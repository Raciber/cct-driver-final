package us.skyguardian.driverns6;

import android.provider.BaseColumns;

public class esquemaChatTable {

    public static abstract class chatEntry implements BaseColumns{

        public static final String TABLE_NAME = "datos_chat";

        public static final String ID_MSG = "id_msg";
        public static final String ID_UPDATE = "id_update";
        public static final String ID_CHANNEL = "id_channel";
        public static final String USER_CHANNEL = "user_channel";
        public static final String NAME_CHAT = "name_chat";

    }

}
