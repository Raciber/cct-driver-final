package us.skyguardian.driverns6;

import android.text.SpannableString;
import android.text.Spanned;

public class ContentItem extends ListItem {

    private SpannableString origen;
    private SpannableString destino;
    private SpannableString fecha;

    public SpannableString getOrigen() {
        return origen;
    }

    public void setOrigen(SpannableString origen) {
        this.origen = origen;
    }

    public SpannableString getDestino() {
        return destino;
    }

    public void setDestino(SpannableString destino) {
        this.destino = destino;
    }

    public SpannableString getFecha() {
        return fecha;
    }

    public void setFecha(SpannableString fecha) {
        this.fecha = fecha;
    }
}