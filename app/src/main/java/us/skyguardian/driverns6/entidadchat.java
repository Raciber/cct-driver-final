package us.skyguardian.driverns6;

public class entidadchat {


    private String id_msg;
    private String id_update;
    private String id_channel;
    private String user_channel;
    private String name_chat;


    public entidadchat(String id_msg, String id_update, String id_channel, String user_channel, String name_chat) {

        this.id_msg = id_msg;
        this.id_update = id_update;
        this.id_channel = id_channel;
        this.user_channel = user_channel;
        this.name_chat = name_chat;
    }



    public String getId_msg() {
        return id_msg;
    }

    public void setId_msg(String id_msg) {
        this.id_msg = id_msg;
    }

    public String getId_update() {
        return id_update;
    }

    public void setId_update(String id_update) {
        this.id_update = id_update;
    }

    public String getId_channel() {
        return id_channel;
    }

    public void setId_channel(String id_channel) {
        this.id_channel = id_channel;
    }

    public String getUser_channel() {
        return user_channel;
    }

    public void setUser_channel(String user_channel) {
        this.user_channel = user_channel;
    }

    public String getName_chat() {
        return name_chat;
    }

    public void setName_chat(String name_chat) {
        this.name_chat = name_chat;
    }
}
