package us.skyguardian.driverns6;

public class Mensaje {
    private String mensaje;
    private String id_mensaje;


    public Mensaje() {
    }

    public Mensaje(String mensaje, String id_mensaje) {
        this.mensaje = mensaje;
        this.id_mensaje = id_mensaje;

    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getId_mensaje() {
        return id_mensaje;
    }

    public void setId_mensaje(String id_mensaje) {
        this.id_mensaje = id_mensaje;
    }


}
