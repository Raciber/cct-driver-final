package us.skyguardian.driverns6;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.GridLayout;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ChatActivity extends AppCompatActivity {

    private ImageButton btnsend;
    private EditText txtSending;
    private RecyclerView rvchat;
    private int valVisibilidad = 0;
    /****************************** DECLARACION DE BOTONES PARA FUNCIONES *************************************************/
    private ImageButton btnComida;
    private ImageButton btnDiesel;
    private ImageButton btnWC;
    private ImageButton btnDescanso;
    private ImageButton btnReparacionMecanica;
    private ImageButton btnTrafico;
    private ImageButton btnEntrega;
    private ImageButton btnRecepcion;
    private ImageButton btnReten;
    private ImageButton btnLlanta;
    /*******************************************************************************/

    private AdapterMensajes adapterMensajes;

    private FirebaseDatabase firebaseDatabase;
    private static DatabaseReference databaseReference;

    private chatDbHelper cdh;
    private SQLiteDatabase db;
    private String auxQuery = "";
    private static String auxQueryID = "";

    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int READ_PHONE_STATE = 107;
    private FusedLocationProviderClient fusedLocationClient;
    String[] linkLocation = {""};
    String[] MsgTexto = {"OPERADOR: REALIZO PARADA POR COMIDA. ",                                      //1
                         "OPERADOR: REALIZO PARADA POR WC. ",                                        //2
                         "OPERADOR: REALIZO PARADA PARA CARGA DE COMBUSTIBLE. ",                      //3
                         "OPERADOR: REALIZO PARADA PARA DESCANSAR. ",                                 //4
                         "OPERADOR: REALIZO PARADA PARA reparación de NEUMATICO. ",                   //5
                         "OPERADOR: REALIZO PARADA PARA REPARACION MECANICA. ",                       //6
                         "OPERADOR: EN ENTREGA DE MERCANCIA. ",                                       //7
                         "OPERADOR: EN RECEPCION DE MERCANCIA. ",                                     //9
                         "OPERADOR: REALIZO PARADA POR RETEN. ",                                      //10
                         "OPERADOR: ME ENCUENTRO EN TRAFICO CARRETERO. ",                             //11
                         "OPERADOR: SOS ENVIADO, ATENTA REACCIÓN, FAVOR DE MARCAR O ACTUAR DE MANERA VELOZ. "}; //12

    String host = "193.193.165.165";
    int port = 20963;

    private String recibeVista="";
    private String recibeUnidad="";
    private LinearLayout vistaActividades;
    private LinearLayout vistaChat1;
    private LinearLayout vistarvchat;
    private LinearLayout vistaChat2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_chat);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        recibeVista = getIntent().getStringExtra("param");
        vistaActividades = findViewById(R.id.xml_LayoutActividadesVista);
        vistaChat1 = findViewById(R.id.xml_LayoutChat1);
        vistaChat2 = findViewById(R.id.xml_LayoutChat2);
        rvchat = findViewById(R.id.xml_rvchat);

        if(recibeVista.equals("1")){
            vistaActividades.setVisibility(View.VISIBLE);
            vistaChat1.setVisibility(View.GONE);
            vistaChat2.setVisibility(View.GONE);
            rvchat.setVisibility(View.GONE);
            recibeUnidad = getIntent().getStringExtra("unit");
        }else{
            if(recibeVista.equals("0")) {
                vistaActividades.setVisibility(View.GONE);
                vistaChat1.setVisibility(View.VISIBLE);
                rvchat.setVisibility(View.VISIBLE);
                vistaChat2.setVisibility(View.VISIBLE);
            }
        }



        cdh = new chatDbHelper(this);
        db = cdh.getWritableDatabase();
        if (db != null) {
            auxQuery = cdh.searchNameChat(1);
            auxQueryID = cdh.searchIdChat(1);//////

            String unitId = "";
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
                if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE))) {
                    //mostrarExplicacionStatePhone();

                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
                }
            }else{
                //El permiso ya fue activado
            }

            if (telephonyManager.getDeviceId() != null) {
                unitId = telephonyManager.getDeviceId();
                System.out.println("IMEI: " + unitId);
            }
            String password = "C0ntr0lCCTSky19";
            MessageSender.initWithHost(host,port,unitId,password);
        } else {
            //System.out.println(.*)
        }



        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        btnsend = (ImageButton) findViewById(R.id.xml_btnsend);
        txtSending = (EditText) findViewById(R.id.xml_txtmsgsending);



        /******************************* INICIALIZACION DE LOS BOTONES DE ACTIVIDADES ********************************************************/
        btnComida = findViewById(R.id.xml_imgBtnFood);
        btnWC = findViewById(R.id.xml_imgBtnWC);
        btnDiesel = findViewById(R.id.xml_imgBtnDiesel);
        btnDescanso = findViewById(R.id.xml_imgBtnDescanso);
        btnLlanta = findViewById(R.id.xml_imgBtnLlanta);
        btnReparacionMecanica = findViewById(R.id.xml_imgBtnReparacionMecanica);

        btnEntrega = findViewById(R.id.xml_imgBtnEntrega);
        btnRecepcion = findViewById(R.id.xml_imgBtnRecepcion);
        btnReten = findViewById(R.id.xml_imgBtnReten);
        btnTrafico = findViewById(R.id.xml_imgBtnTrafico);


        /***************************************************************************************/

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference(auxQuery); //Nombre de la sala de chat donde se guardaran todos los mensajes.

        adapterMensajes = new AdapterMensajes(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvchat.setLayoutManager(linearLayoutManager);
        rvchat.setAdapter(adapterMensajes);

        /********************************* AGREGA FUNCION DE LOS BOTONES DE FUNCIONES PRINCIPALES *************************************************************/



        /*btnStops.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valVisibilidad == 0) {
                    gridLayoutSecond.setVisibility(View.VISIBLE);
                    valVisibilidad = 1;
                } else {
                    if (valVisibilidad == 1) {
                        gridLayoutSecond.setVisibility(View.GONE);
                        valVisibilidad = 0;
                    }
                }
            }
        });*/

        /********************************* AGREGA FUNCION DE LOS BOTONES DE ACTIVIDADES SECUNDARIOS *************************************************************/
        btnComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[0], 1);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnWC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[1],2);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnDiesel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[2],3);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnDescanso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[3],4);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnLlanta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[4],5);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnReparacionMecanica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[5],6);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });



        btnEntrega.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[6],7);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnRecepcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[7],8);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnReten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[8],9);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btnTrafico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation(MsgTexto[9],10);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        /************************************************************************************************/

        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    String auxtxt = txtSending.getText().toString();
                    if(!(auxtxt.equals(""))) {
                        new sendMessageTelegram().execute("OPERADOR: " + auxtxt);
                    }
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(ChatActivity.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        adapterMensajes.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                setScroll();
            }
        });

        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                MensajeRecibir m = dataSnapshot.getValue(MensajeRecibir.class);
                adapterMensajes.addMensaje(m);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private int verificarSenal(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

    private void setScroll() {
        rvchat.scrollToPosition(adapterMensajes.getItemCount() - 1);
    }

    private void accederPermisoLocation(final String mensaje, final int validaActividad) {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje
                    mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }

            } else {
                //Si el permiso fue activado llamo al metodo de ubicacion
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();

                            new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + linkLocation[0]);

                            new registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),mensaje);

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                    .batteryLevel((byte)batLevel)
                                    .text("Actividad enviada: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Actividad enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío de la actividad.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });

                            if(validaActividad == 3){
                                    Intent intent = new Intent (ChatActivity.this, registerFuel.class);
                                    intent.putExtra("unit", recibeUnidad);
                                    intent.putExtra("x", location.getLongitude());
                                    intent.putExtra("y", location.getLatitude());
                                    //startActivity
                                    startActivity(intent);
                            }
                        }
                        else{
                            new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + "Ubicación desconocida");
                            new registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,"0","0",mensaje);

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);


                            Message message = new Message()
                                    .time(new Date().getTime())
                                    //.location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                    .batteryLevel((byte)batLevel)
                                    .text("Actividad enviada: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Actividad enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío de la actividad.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                            if(validaActividad == 3) {
                                Intent intent = new Intent(ChatActivity.this, registerFuel.class);
                                intent.putExtra("unit", recibeUnidad);
                                intent.putExtra("x", 0);
                                intent.putExtra("y", 0);
                                startActivity(intent);
                            }
                        }
                    }
                });
            }

        } else {//Si la API es menor a 23 - llamo al metodo de ubicacion
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        //System.out.println(.*)
                        linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();
                        new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + linkLocation[0]);
                        new registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),mensaje);
                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .batteryLevel((byte)batLevel)
                                .text("Actividad enviada: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Actividad enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío de la actividad.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                        if(validaActividad == 3) {
                            Intent intent = new Intent(ChatActivity.this, registerFuel.class);
                            intent.putExtra("unit", recibeUnidad);
                            intent.putExtra("x", location.getLongitude());
                            intent.putExtra("y", location.getLatitude());
                            startActivity(intent);
                        }
                    }
                    else{
                        new sendMessageTelegram().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mensaje + "Ubicación desconocida");
                        new registerEvent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,recibeUnidad,"0","0",mensaje);
                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                //.location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .batteryLevel((byte)batLevel)
                                .text("Actividad enviada: \n"+mensaje + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Actividad enviada con éxito a monitoreo.\n"+mensaje,Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío de la actividad.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });

                        if(validaActividad == 3) {
                            Intent intent = new Intent(ChatActivity.this, registerFuel.class);
                            intent.putExtra("unit", recibeUnidad);
                            intent.putExtra("x", 0);
                            intent.putExtra("y", 0);
                            startActivity(intent);
                        }
                    }
                }
            });
        }
    }

    private void mostrarExplicacion() {
        new AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la ultima y actual ubicación del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    public void mensajeAccionCancelada() {
        Toast.makeText(getApplicationContext(), "Haz rechazado la petición, puede suceder que la app no trabaje de manera adecuada.", Toast.LENGTH_SHORT).show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_COARSE:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accederPermisoLocation(MsgTexto[0],1);
                } else {
                    mostrarExplicacion();
                }
             break;
            case LOCATION_FINE:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accederPermisoLocation(MsgTexto[0],1);
                } else {
                    mostrarExplicacion();
                }
                break;
        }
    }

    public class sendMessageTelegram extends AsyncTask<String, Void, String> {

        Response response;
        String respuestabody = "";

        @Override
        protected String doInBackground(String... params) {

            double auxID = Double.parseDouble(auxQueryID);
            ////System.out.println(.*)
            ////System.out.println(.*)

            OkHttpClient client = new OkHttpClient();
            //client.connectTimeoutMillis();

            HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.telegram.org/bot748803453:AAGXb7v2OrmbZdkwMJcmaXVBeVe6xGLvWDU/sendMessage").newBuilder();
            urlBuilder.addQueryParameter("chat_id", BigDecimal.valueOf(auxID).toString());
            urlBuilder.addQueryParameter("text", params[0]);

            String url = urlBuilder.build().toString();

            Request request = new Request.Builder()
                    .url(url)
                    .build();

            try {
                response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    respuestabody = response.body().string();
                   return respuestabody.toString();
                }else{

                    respuestabody = response.body().string();
                    return respuestabody.toString();
                }
            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println(s);

            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean code = jsonObject.getBoolean("ok");
                if(code == true){

                    databaseReference.push().setValue(new MensajeEnviar(jsonObject.getJSONObject("result").getString("text"), jsonObject.getJSONObject("result").getString("message_id"), ServerValue.TIMESTAMP));
                    txtSending.setText("");
                }
                else{
                    Toast.makeText(getApplication(),"Error al enviar el mensaje, intente de nuevo",Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public class registerEvent extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("unit_x", strings[1]);
                paramObject.put("unit_y", strings[2]);
                paramObject.put("unit_txt", strings[3]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            client.connectTimeoutMillis();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/event")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println(.*)
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //System.out.println(.*)
            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){

                }else{
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en el servidor, para registro de evento.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }
}
