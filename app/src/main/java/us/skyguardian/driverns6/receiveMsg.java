package us.skyguardian.driverns6;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class receiveMsg extends Service {

    private Timer temporizador = new Timer();
    private static final long INTERVALO_ACTUALIZACION = 30000; // En ms
    private double cronometro = 0;
    private String datos = "";

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    private chatDbHelper cdh;
    private SQLiteDatabase db;
    private String auxQuery = "";

    // Framgmento para las notificaciones
    NotificationCompat.Builder mBuilder;
    int mNotificationId = 1;
    // Id del canal para la notificacion
    String channelId = "my_channel_01";

    // Puerto e IP para el uso de wiatag.
    String host = "193.193.165.165";
    int port = 20963;
    private static final int READ_PHONE_STATE = 107;

    String unitId = "";
    String password = "";


    private FusedLocationProviderClient fusedLocationClient;

    @Override
    public void onCreate() {
        super.onCreate();


        iniciarCronometro();
    }

    @Override
    public void onDestroy() {
        pararCronometro();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void iniciarCronometro() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        cdh = new chatDbHelper(this);
        db = cdh.getWritableDatabase();
        temporizador.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                cronometro += 0;
                //System.out.println(.*)
                int validaInternet = verificarSenal();

                if (db != null) {
                    auxQuery = cdh.searchNameChat(1);
                    if (auxQuery.equals("0")) {
                        //System.out.println(.*)
                        //cdh.addNameChat(numeroviaje);
                    } else {
                        if (validaInternet == 1) {
                            Intent intent = new Intent("intentKey");
                            // You can also include some extra data.
                            intent.putExtra("keyinternet", "");
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            fusedLocationClient = LocationServices.getFusedLocationProviderClient(receiveMsg.this);
                            firebaseDatabase = FirebaseDatabase.getInstance();
                            databaseReference = firebaseDatabase.getReference(auxQuery); //Nombre de la sala de chat donde se guardaran todos los mensajes.
                            getMsgTelegram();
                            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            if (telephonyManager.getDeviceId() != null) {
                                unitId = telephonyManager.getDeviceId();
                                System.out.println("IMEI: " + unitId);
                            }
                            password = "C0ntr0lCCTSky19";
                        }else{
                            if(validaInternet == 0){
                                System.out.println("Se ha perdido la señal.");
                                Intent intent = new Intent("intentKey");
                                // You can also include some extra data.
                                intent.putExtra("keyinternet", "Se ha perdido la señal, buscando conexión a internet...");
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                            }
                        }
                    }
                }else{
                    //System.out.println(.*)
                }


            }
        }, 0, INTERVALO_ACTUALIZACION);
    }

    private void pararCronometro() {
        if (temporizador != null)
            temporizador.cancel();
    }

    private int verificarSenal(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

    private void getMsgTelegram(){

        Response response;
        String respuestabody = "";

        OkHttpClient client = new OkHttpClient();

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.telegram.org/bot748803453:AAGXb7v2OrmbZdkwMJcmaXVBeVe6xGLvWDU/getUpdates").newBuilder();

        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
            response = client.newCall(request).execute();
            if(response.isSuccessful()){
                respuestabody = response.body().string();
                responseTelegram(respuestabody.toString());
            }else{

                respuestabody = response.body().string();
                responseTelegram(respuestabody.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void responseTelegram (String str){
        String sign_chat="";
        try {
            System.out.println("*");
            JSONObject jo = new JSONObject(str);
            if(jo.getString("ok") == "true"){
                if(jo.getJSONArray("result").length() == 0){
                    //System.out.println(.*)
                }else{
                    ////System.out.println(.*)
                    for(int i=0;i<jo.getJSONArray("result").length(); i++){
                        if(!(jo.getJSONArray("result").getJSONObject(i).getJSONObject("channel_post").isNull("text"))) {
                            if(jo.getJSONArray("result").getJSONObject(i).getJSONObject("channel_post").getJSONObject("chat").getString("title").equals(auxQuery)){

                                ////System.out.println(.*)
                                String idUpdateMesg = jo.getJSONArray("result").getJSONObject(i).getString("update_id");

                                ////System.out.println(.*)
                                int id_message = jo.getJSONArray("result").getJSONObject(i).getJSONObject("channel_post").getInt("message_id");

                                ////System.out.println(.*)
                                double id_chat = jo.getJSONArray("result").getJSONObject(i).getJSONObject("channel_post").getJSONObject("chat").getDouble("id");

                                ////System.out.println(.*)
                                String userChat = jo.getJSONArray("result").getJSONObject(i).getJSONObject("channel_post").getJSONObject("chat").getString("title");

                                ////System.out.println(.*)
                                String text_chat = jo.getJSONArray("result").getJSONObject(i).getJSONObject("channel_post").getString("text");

                                if(!(jo.getJSONArray("result").getJSONObject(i).getJSONObject("channel_post").isNull("author_signature"))){
                                    sign_chat = jo.getJSONArray("result").getJSONObject(i).getJSONObject("channel_post").getString("author_signature");
                                }



                                /* Valida si existe o cambia el ID del CHAT */
                                String auxQueryIdChat = cdh.searchIdChat(1);
                                if(auxQueryIdChat.equals(id_chat)){
                                    //System.out.println(.*)
                                }else{
                                    int valida = cdh.updateIdChat(String.valueOf(id_chat), 1);
                                    if(valida == 1){
                                        //System.out.println(.*)

                                    }else{
                                        //System.out.println(.*)
                                    }
                                }

                                /* Consulta para verificar ID de chat en BD interna */
                                String auxQuery = cdh.searchIdChatUpdate(1);

                                //System.out.println(.*)
                                //System.out.println(.*)
                                int idBd = Integer.parseInt(auxQuery);
                                int idTelegram = Integer.parseInt(idUpdateMesg);



                                if(auxQuery.equals(idUpdateMesg)){
                                    //System.out.println(.*)
                                }
                                else{
                                    if(idBd<idTelegram){
                                        String nameChat = cdh.searchNameChat(1);
                                        if (userChat.equals(nameChat)) {
                                            // Empieza el fragmento de notificaciones

                                            Intent intent = new Intent(this, ChatActivity.class);
                                            intent.putExtra("param", "0"); //If you wan to send data also
                                            PendingIntent pIntent = PendingIntent.getActivity(this, mNotificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                                            NotificationManager notificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
                                            mBuilder = new NotificationCompat.Builder(getApplicationContext(), null);
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                                // Nombre del canal al usuario visible
                                                CharSequence name = "Mensajes";
                                                // Descripcion del canal
                                                String description = "Aviso de mensajes";
                                                int importance = NotificationManager.IMPORTANCE_HIGH;

                                                NotificationChannel mChannel = new NotificationChannel(channelId, name, importance);
                                                //Configuracion del canal de notificacion
                                                mChannel.setDescription(description);
                                                mChannel.enableLights(true);

                                                //Ajustamos el color de luz de led si el dispositivo lo soporta
                                                mChannel.setLightColor(Color.GREEN);
                                                mChannel.enableVibration(true);
                                                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

                                                notificationManager.createNotificationChannel(mChannel);

                                                mBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId);
                                            }

                                            mBuilder.setSmallIcon(R.drawable.ic_stat_name)
                                                    .setContentIntent(pIntent)
                                                    .setContentTitle("CCT Driver")
                                                    .setContentText(sign_chat+": "+text_chat)
                                                    //.addAction(new NotificationCompat.Action.Builder(1, "Ver mensaje", pIntent).build())
                                                    .setAutoCancel(true)
                                                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));


                                            notificationManager.notify(mNotificationId, mBuilder.build());



                                            int valida = cdh.updateIdChatUpdate(idUpdateMesg, 1);
                                            if(valida == 1){
                                                //System.out.println(.*)
                                                databaseReference.push().setValue(new MensajeEnviar(sign_chat + ": " + text_chat, String.valueOf(id_message), ServerValue.TIMESTAMP));
                                            }else{
                                                //System.out.println(.*)
                                            }

                                        }else{
                                            //System.out.println(.*)
                                        }
                                    }
                                }
                                /****************************************************************************************************************************************************/
                            }
                        }
                    }
                }

            }else{
                //System.out.println(.*)
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        //getLatReportCustodia();

    }

    @SuppressLint("MissingPermission")
    private void getLatReportCustodia(){
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<android.location.Location>() {
            @Override
            public void onSuccess(android.location.Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    //System.out.println(.*)
                    // Logic to handle location object
                    //System.out.println(.*)
                    //linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();
                    //new registerEvent().execute(idUnit,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),"OPERADOR: SOS ENVIADO, ATENTA REACCIÓN, FAVOR DE MARCAR O ACTUAR DE MANERA VELOZ.");
                    BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                    int batLevel = 0;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                    }
                    //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                    //location.
                    MessageSender.initWithHost(host,port,unitId,password);
                    Message message = new Message()
                            .time(new Date().getTime())
                            .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                            //.Sos()

                            .batteryLevel((byte)batLevel);
                            //.text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                    MessageSender.sendMessage(message,new MessageSenderListener()
                    {
                        @Override
                        protected void onSuccess() {
                            super.onSuccess();
                            //System.out.println(.*)
                            /*runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                }
                            });*/
                        }
                        @Override
                        protected void onFailure(byte errorCode) {
                            super.onFailure(errorCode);
                            //System.out.println(.*)
                            /*runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                }
                            });*/
                        }
                    });
                }
                else{
                    //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + "Ubicación desconocida");
                    //linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();
                    BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                    int batLevel = 0;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                    }
                    //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                    MessageSender.initWithHost(host,port,unitId,password);
                    Message message = new Message()
                            .time(new Date().getTime())
                            //.location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                            //.Sos()
                            .batteryLevel((byte)batLevel);
                            //.text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                    MessageSender.sendMessage(message,new MessageSenderListener()
                    {
                        @Override
                        protected void onSuccess() {
                            super.onSuccess();
                            //System.out.println(.*)
                                /*receiveMsg.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(), "Mensaje SOS enviado.", Toast.LENGTH_LONG).show();
                                    }
                                });*/

                        }
                        @Override
                        protected void onFailure(byte errorCode) {
                            super.onFailure(errorCode);
                            //System.out.println(.*)
                            /*runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                }
                            });*/
                        }
                    });
                }
            }
        });
    }
}