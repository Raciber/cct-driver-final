package us.skyguardian.driverns6;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyRecyclerAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    //Header header;
    List<ListItem> list;
    public MyRecyclerAdapter(List<ListItem> headerItems) {
        this.list = headerItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            View v = inflater.inflate(R.layout.header_layout, parent, false);
            return  new VHHeader(v);
        } else {
            View v = inflater.inflate(R.layout.cards_layout_agenda, parent, false);
            return new VHItem(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            // VHHeader VHheader = (VHHeader)holder;
            Header  currentItem = (Header) list.get(position);
            VHHeader VHheader = (VHHeader)holder;
            VHheader.txtTitle.setText(currentItem.getHeader());
        } else if (holder instanceof VHItem) {
            ContentItem currentItem = (ContentItem) list.get(position);
            VHItem VHitem = (VHItem) holder;
            VHitem.txtNameFecha.setText(currentItem.getFecha());
            VHitem.txtNameOrigen.setText(currentItem.getOrigen());
            VHitem.txtNameDestino.setText(currentItem.getDestino());
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return list.get(position) instanceof Header;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

class VHHeader extends RecyclerView.ViewHolder{
    TextView txtTitle;
    public VHHeader(View itemView) {
        super(itemView);
        this.txtTitle = (TextView) itemView.findViewById(R.id.header_id);
    }
}
class VHItem extends RecyclerView.ViewHolder{
    TextView txtNameOrigen;
    TextView txtNameDestino;
    TextView txtNameFecha;
    public VHItem(View itemView) {
        super(itemView);
        this.txtNameFecha = itemView.findViewById(R.id.item_content);
        this.txtNameOrigen =  itemView.findViewById(R.id.item_content2);
        this.txtNameDestino = itemView.findViewById(R.id.item_content3);
    }
}
