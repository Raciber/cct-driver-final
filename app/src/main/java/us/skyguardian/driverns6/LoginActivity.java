package us.skyguardian.driverns6;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A login screen that offers login via phone.
 */
public class LoginActivity extends AppCompatActivity {

    // UI references.
    // Botones de inicio de sesión.
    private Button btncelular;
    private Button btnclave;
    private Button btnReturnSess;
    //EditText para insertar clave o numero de celular.
    private EditText txtphonelogin;
    private EditText txtclavelogin;
    //Botones de acciones
    private Button btnlogin;
    private Button btnAgenda;
    //Layout de contenido
    private View layoutSesiones;
    private View layoutClave;
    private View layoutPhone;
    private View layoutAcciones;

    //private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private TextView lblloading;
    Integer counter = 1;

    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int CAMERA = 103;
    private static final int WRITE_EXTERNAL_STORAGE_CODE = 104;
    private static final int READ_EXTERNAL_STORAGE_CODE = 105;
    private static final int CALLPHONE_CODE = 106;
    private static final int READ_PHONE_STATE = 107;

    Integer validaInicioSesion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_login);

        //progressBar = findViewById(R.id.progressBar);
        //progressBar.setMax(10);

        lblloading = findViewById(R.id.xml_lblload);

        btncelular = findViewById(R.id.xml_btnSesionPhone);
        btnclave = findViewById(R.id.xml_btnSesionClave);
        btnReturnSess = findViewById(R.id.xml_btnReturnSesiones);
        btnReturnSess.setPaintFlags(btnReturnSess.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        // Set up the login form.
        txtclavelogin = findViewById(R.id.xml_txtkeylogin);
        txtphonelogin = findViewById(R.id.xml_txtphonelogin);

        btnlogin = findViewById(R.id.xml_btnlogin);
        btnAgenda = findViewById(R.id.xml_btnAgenda);

        layoutSesiones = findViewById(R.id.xml_layout_sesiones);
        layoutPhone = findViewById(R.id.xml_layout_txtphone);
        layoutClave = findViewById(R.id.xml_layout_txtclave);
        layoutAcciones = findViewById(R.id.xml_layout_acciones);

        //Muestra el txt para iniciar sesion con el numero de celular
        btncelular.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Contenedores
                layoutSesiones.setVisibility(View.GONE);
                layoutPhone.setVisibility(View.VISIBLE);
                layoutAcciones.setVisibility(View.VISIBLE);

                validaInicioSesion = 1;
                //Cajas de texto
                //txtphonelogin.setVisibility(View.VISIBLE);
            }
        });

        btnclave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Contenedores
                layoutSesiones.setVisibility(View.GONE);
                layoutClave.setVisibility(View.VISIBLE);
                layoutAcciones.setVisibility(View.VISIBLE);

                validaInicioSesion = 2;
                //Cajas de texto
                //txtclavelogin.setVisibility(View.VISIBLE);

            }
        });

        btnReturnSess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Contenedores
                layoutSesiones.setVisibility(View.VISIBLE);
                layoutClave.setVisibility(View.GONE);
                layoutPhone.setVisibility(View.GONE);
                layoutAcciones.setVisibility(View.GONE);

                txtclavelogin.setText("");
                txtphonelogin.setText("");

                validaInicioSesion = 0;
                //Cajas de texto
                //txtphonelogin.setVisibility(View.GONE);
                //txtclavelogin.setVisibility(View.GONE);
            }
        });

        btnlogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validaInicioSesion == 1) {
                    validaPhone();
                }else{
                    if(validaInicioSesion == 2){
                        validaClave();
                    }
                }
            }
        });

        btnAgenda.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validaInicioSesion == 1) {
                    if (txtphonelogin.getText().toString().trim().equalsIgnoreCase("")) {
                        txtphonelogin.setError("Este campo no puede estar vacio.");

                    }else{
                        Intent intent = new Intent(LoginActivity.this, AgendaActivity.class);
                        intent.putExtra("tellogin", txtphonelogin.getText().toString());
                        intent.putExtra("valida", "1");
                        startActivity(intent);
                    }

                }else{
                    if(validaInicioSesion == 2){
                        if (txtclavelogin.getText().toString().trim().equalsIgnoreCase("")) {
                            txtclavelogin.setError("Este campo no puede estar vacio.");

                        }else{
                            Intent intent = new Intent(LoginActivity.this, AgendaActivity.class);
                            intent.putExtra("keylogin", txtclavelogin.getText().toString());
                            intent.putExtra("valida", "2");
                            startActivity(intent);
                        }

                    }
                }
            }
        });



        accederPermisoLocation();
        //accedePermisoPhoto();
        //accedePermisoWriteStorage();
        //accedePermisoReadStorage();
    }

    public void validaClave(){
        if (txtclavelogin.getText().toString().trim().equalsIgnoreCase("")) {
            txtclavelogin.setError("Este campo no puede estar vacio.");

        }else{

            String auxclave = txtclavelogin.getText().toString().trim();
            //System.out.println(.*)
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                new getTripsForKeyTask().execute(auxclave);
                //Toast.makeText(this,"Sesion con clabe: " + auxclave,Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this,"No tienes conexión a internet, revisa la misma y vuelve a ingresar.",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void validaPhone(){
        if (txtphonelogin.getText().toString().trim().equalsIgnoreCase("")) {
            txtphonelogin.setError("Este campo no puede estar vacio.");

        }else{


            String auxphone = "+52"+txtphonelogin.getText().toString().trim();
            //System.out.println(.*)
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                new getTripsForPhoneTask().execute(auxphone);
            } else {
                Toast.makeText(this,"No tienes conexión a internet, revisa la misma y vuelve a ingresar.",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        AlertDialog.Builder mensaje=new AlertDialog.Builder(this);
        mensaje.setTitle("No puedes salir de esta aplicación, por seguridad.");
        mensaje.setCancelable(false);
        /*mensaje.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });*/
        mensaje.setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        mensaje.show();
    }



    private class getTripsForPhoneTask extends AsyncTask <String, Void, String>{

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            counter = 1;
            /*progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);
            lblloading.setVisibility(View.VISIBLE);*/
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Verificando viajes...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("telefono", params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/get/phonedriver")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.code() == 404){
                    saveResponse =  response.body().string().toString();
                }else {
                    if(response.code() == 400){
                        saveResponse =  response.body().string().toString();
                    }else{
                        if(response.isSuccessful()){
                            saveResponse =  response.body().string().toString();
                        }else{
                            saveResponse = response.body().string().toString();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            /*progressBar.setVisibility(View.GONE);
            lblloading.setVisibility(View.GONE);*/


            try {
                if(result.isEmpty()){
                    progressDialog.cancel();
                    String msg = "Error en número celular,verifique e intentelo de nuevo.";
                    Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
                }else {
                    JSONObject jo = new JSONObject(result);
                    String success = jo.getString("success");

                    if (success != "false") {
                        progressDialog.cancel();
                        Intent intent = new Intent(LoginActivity.this, actividades.class);
                        intent.putExtra("param", result);
                        startActivity(intent);
                    } else {
                        progressDialog.cancel();
                        String msg = jo.getString("message");
                        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
                    }
                }


            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Upss, ocurrio un error intentalo de nuevo.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }



            //System.out.println(.*)

        }
    }

    private class getTripsForKeyTask extends AsyncTask <String, Void, String>{

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            counter = 1;
            /*progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);
            lblloading.setVisibility(View.VISIBLE);*/
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Verificando viajes...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("llave", params[0]);
                System.out.println(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/get/keydriver")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.code() == 404){
                    saveResponse =  response.body().string().toString();
                }else {
                    if(response.code() == 400){
                        saveResponse =  response.body().string().toString();
                    }else{
                        if(response.isSuccessful()){
                            saveResponse =  response.body().string().toString();
                        }else{
                            saveResponse = response.body().string().toString();
                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println(result);
            /*progressBar.setVisibility(View.GONE);
            lblloading.setVisibility(View.GONE);*/

            try {
                if(result.isEmpty()){
                    progressDialog.cancel();
                    String msg = "Error en llave móvil, verifique e intentelo de nuevo.";
                    Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
                }else{
                    JSONObject jo = new JSONObject(result);
                    String success = jo.getString("success");

                    if(success != "false"){
                        progressDialog.cancel();
                        Intent intent = new Intent (LoginActivity.this, actividades.class);
                        intent.putExtra("param", result);
                        startActivity(intent);
                    }else{
                        progressDialog.cancel();
                        String msg = jo.getString("message");
                        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Upss, ocurrio un error intentalo de nuevo.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }



            //System.out.println(.*)

        }
    }


    private void accederPermisoLocation() {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje
                    mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);

                }

            } else {

            }

        }
        accedePermisoPhoto();
    }

    private void accedePermisoPhoto(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))) {
                mostrarExplicacionPhoto();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA);
            }
        }else{
            //El permiso ya fue activado
        }
        accedePermisoWriteStorage();
    }

    private void accedePermisoWriteStorage(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE))) {
                mostrarExplicacionWriteStorage();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_CODE);
            }
        }else{
            //El permiso ya fue activado
        }
        accedePermisoReadStorage();
    }

    private void accedePermisoReadStorage(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE))) {
                mostrarExplicacionReadStorage();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_CODE);
            }
        }else{
            //El permiso ya fue activado
        }
        accedePermisoCallphone();
    }

    private void accedePermisoCallphone(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE))) {
                mostrarExplicacionCallPhone();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, CALLPHONE_CODE);
            }
        }else{
            //El permiso ya fue activado
        }
        accedePermisoStatePhone();
    }

    private void accedePermisoStatePhone(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE))) {
                mostrarExplicacionStatePhone();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
            }
        }else{
            //El permiso ya fue activado
        }
        //accedePermisoWriteStorage();
    }




    private void mostrarExplicacion() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la ultima y actual ubicación del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionPhoto() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la camara del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.CAMERA}, CAMERA);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionWriteStorage() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder al almacenamiento del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_CODE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionReadStorage() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para lectura al almacenamiento del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE_CODE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionCallPhone() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para realizar llamadas en este dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.CALL_PHONE}, CALLPHONE_CODE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    private void mostrarExplicacionStatePhone() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para realizar configuración en este dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    public void mensajeAccionCancelada() {
        Toast.makeText(getApplicationContext(), "Haz rechazado la petición, puede suceder que la app no trabaje de manera adecuada.", Toast.LENGTH_SHORT).show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_COARSE:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accederPermisoLocation();
                } else {
                }
                break;
            case LOCATION_FINE:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accederPermisoLocation();
                } else {
                }
                break;
            case CAMERA:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoPhoto();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;

            case WRITE_EXTERNAL_STORAGE_CODE:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoWriteStorage();
                } else {

                }
                break;

            case READ_EXTERNAL_STORAGE_CODE:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoReadStorage();
                } else {
                }
                break;

            case CALLPHONE_CODE:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoCallphone();

                } else {
                }
                break;

            case READ_PHONE_STATE:
                //System.out.println(.*)
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accedePermisoStatePhone();

                } else {
                }
                break;
        }
    }
}

