package us.skyguardian.driverns6;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class registerFuel extends AppCompatActivity {

    private Button btnPayCash;
    private Button btnPayOnecard;

    private EditText txtOdometro;
    private EditText txtFactura;
    private EditText txtLitros;

    private RadioGroup radioGroup;
    private String typeFuel="";

    private String strDisponibleCard="";

    private String receiveUnit;
    private Double receiveLon;
    private Double receiveLat;

    private TextView lblDisponible;

    /* Progress Dialog */
    private ProgressDialog progressDialog;

    /* Para carga de disponible */
    private ProgressBar progressBar;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_register_fuel);

        receiveUnit = getIntent().getStringExtra("unit");
        receiveLon = getIntent().getExtras().getDouble("x");
        receiveLat = getIntent().getExtras().getDouble("y");

        btnPayCash = findViewById(R.id.xml_btnPayCash);
        btnPayOnecard = findViewById(R.id.xml_btnPayCard);

        txtOdometro = findViewById(R.id.xml_txtOdometro);
        txtFactura = findViewById(R.id.xml_txtFactura);
        txtLitros = findViewById(R.id.xml_txtLitros);

        radioGroup = findViewById(R.id.xml_RadioGroup);

        lblDisponible = findViewById(R.id.xml_lblDisponible);

        progressBar = findViewById(R.id.xml_progressDisponible);
        progressBar.setMax(10);

        //new getCashCard().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, receiveUnit);
        //new getOdometro().executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, receiveUnit);
        new getCashCard().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, receiveUnit);

        new getOdometro().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, receiveUnit);
        //new getCashCard().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, receiveUnit);
        //new getOdometro().execute(receiveUnit);

         //.execute(receiveUnit);

        //intent = new Intent(this, actividades.class);

        /*txtLitros.addTextChangedListener(new TextWatcher() {
            private String current = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                txtLitros.setText(s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //txtLitros.setText(s.toString());
                txtLitros.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                        //originalString = originalString.replaceAll("/\\D/g", "").replaceAll("/([0-9]{0})([0-9]{2})$/","$1.$2").replaceAll("/\\B(?=(\\d{10})+(?!\\d)\\.?)/g",",");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###.##");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    System.out.println(formattedString);
                    txtLitros.setText(formattedString);
                    txtLitros.setSelection(txtLitros.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                txtLitros.addTextChangedListener(this);

            }
        });*/

        btnPayCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtOdometro.getText().toString().trim().equalsIgnoreCase("")){
                    txtOdometro.setError("Este campo no puede estar vacio.");
                }else{
                    if(txtFactura.getText().toString().trim().equalsIgnoreCase("")){
                        txtFactura.setError("Este campo no puede estar vacio.");
                    }else{
                        if(txtLitros.getText().toString().trim().equalsIgnoreCase("")){
                            txtLitros.setError("Este campo no puede estar vacio.");
                        }else{
                            typeFuel = verifiedTypeFuel();
                            if(typeFuel != null){
                                //Toast.makeText(registerFuel.this,typeFuel,Toast.LENGTH_LONG).show();
                                msgRegisterWithoutCard();
                            }else{
                                Toast.makeText(registerFuel.this,"Selecciona un tipo de combustible.",Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }
        });

        btnPayOnecard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtOdometro.getText().toString().trim().equalsIgnoreCase("")){
                    txtOdometro.setError("Este campo no puede estar vacio.");
                }else{
                    if(txtFactura.getText().toString().trim().equalsIgnoreCase("")){
                        txtFactura.setError("Este campo no puede estar vacio.");
                    }else{
                        if(txtLitros.getText().toString().trim().equalsIgnoreCase("")){
                            txtLitros.setError("Este campo no puede estar vacio.");
                        }else{
                            typeFuel = verifiedTypeFuel();
                            if(typeFuel != null){
                                //Toast.makeText(registerFuel.this,typeFuel,Toast.LENGTH_LONG).show();
                                msgRegisterWithCard();
                            }else{
                                Toast.makeText(registerFuel.this,"Selecciona un tipo de combustible.",Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }
        });

    }

    public String verifiedTypeFuel(){
        if(radioGroup.getCheckedRadioButtonId() == R.id.xml_RadioBtnPremium){
            return "Combustible: Premium ";
        }else{
            if(radioGroup.getCheckedRadioButtonId() == R.id.xml_RadioBtnRegular){
                return "Combustible: Regular ";
            }else{
                if(radioGroup.getCheckedRadioButtonId() == R.id.xml_RadioBtnDiesel){
                    return "Combustible: Diesel ";
                }
            }
        }
        return null;
    }

    private void msgRegisterWithoutCard() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Aviso")
                .setMessage("¿Desea guardar este registro de combustible?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String texto = typeFuel+txtOdometro.getText().toString()+"Km";
                        //System.out.println(.*)
                        new setRegisterFuelWithoutCard().execute(receiveUnit,txtLitros.getText().toString(),txtFactura.getText().toString(),String.valueOf(receiveLat),String.valueOf(receiveLon),texto);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .show();
    }

    private class setRegisterFuelWithoutCard extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(registerFuel.this);
            progressDialog.setMessage("Registrando carga...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("volume", strings[1]);
                paramObject.put("cost", strings[2]);
                paramObject.put("unit_x", strings[3]);
                paramObject.put("unit_y", strings[4]);
                paramObject.put("unit_txt", strings[5]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            client.connectTimeoutMillis();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/fuelevent")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }


        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //System.out.println(.*)

            try {
                JSONObject jo = new JSONObject(s);
                if(jo.length() != 0){
                    String success = jo.getString("success");

                    if(success != "false"){
                        progressDialog.cancel();
                        String msg = jo.getString("evento");
                        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                        registerFuel.this.finish();
                        intent = new Intent(registerFuel.this, ChatActivity.class);
                        intent.putExtra("param", "1");
                        intent.putExtra("unit",receiveUnit);
                        startActivity(intent);
                    }else{
                        progressDialog.cancel();
                        String msg = jo.getString("message");
                        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                    }
                }



            } catch (JSONException e) {
                progressDialog.cancel();
                Toast.makeText(getBaseContext(),"Error en servidor para registro de carga.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private void msgRegisterWithCard() {
        new android.app.AlertDialog.Builder(this)
                .setTitle("Aviso")
                .setMessage("¿Desea guardar este registro de combustible y activar tarjeta de combustible?")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String texto = typeFuel+txtOdometro.getText().toString()+"Km";
                        //System.out.println(.*)
                        new setRegisterFuelWithCard().execute(receiveUnit,txtLitros.getText().toString(),txtFactura.getText().toString(),String.valueOf(receiveLat),String.valueOf(receiveLon),texto);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .show();
    }

    private class getCashCard extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setProgress(0);

        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            client.connectTimeoutMillis();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/get/cashcard")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            System.out.println(s);

            try {
                JSONObject jo = new JSONObject(s);
                if(jo.length() != 0){
                    String success = jo.getString("success");

                    if(success != "false"){
                        //progressDialog.cancel();
                        progressBar.setVisibility(View.GONE);

                        if(!(jo.isNull("message"))){
                            Toast.makeText(getBaseContext(),jo.getString("message"),Toast.LENGTH_LONG).show();
                            lblDisponible.setVisibility(View.VISIBLE);
                            lblDisponible.setText("Disponible: $" + jo.getString("message"));
                            //registerFuel.this.finish();
                            //intent = new Intent(registerFuel.this, ChatActivity.class);
                            //intent.putExtra("param", "1");
                            //intent.putExtra("unit",receiveUnit);
                            //startActivity(intent);
                        }else{
                            String msg = jo.getString("message");
                            Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                        }
                    }else{
                        //progressDialog.cancel();

                        progressBar.setVisibility(View.GONE);
                        String msg = jo.getString("message");
                        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                    }
                }



            } catch (JSONException e) {
                System.out.println("Error " + e);
                Toast.makeText(getBaseContext(),"Error en servidor para obtener saldo de tarjeta.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }
    }

    private class setRegisterFuelWithCard extends AsyncTask<String, Void, String> {

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(registerFuel.this);
            progressDialog.setMessage("Activando tarjeta...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("volume", strings[1]);
                paramObject.put("cost", strings[2]);
                paramObject.put("unit_x", strings[3]);
                paramObject.put("unit_y", strings[4]);
                paramObject.put("unit_txt", strings[5]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            client.connectTimeoutMillis();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/fuelevent/card")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }


        protected void onProgressUpdate(Integer... values) {
            //super.onProgressUpdate(values);
            progressDialog.setProgress((int)(values[0]));
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //System.out.println(.*)

            try {
                JSONObject jo = new JSONObject(s);
                if(jo.length() != 0){
                    String success = jo.getString("success");

                    if(success != "false"){
                        progressDialog.cancel();

                        if(!(jo.isNull("status"))){
                            Toast.makeText(getBaseContext(),jo.getString("status"),Toast.LENGTH_LONG).show();
                            registerFuel.this.finish();
                            intent = new Intent(registerFuel.this, ChatActivity.class);
                            intent.putExtra("param", "1");
                            intent.putExtra("unit",receiveUnit);
                            startActivity(intent);
                        }else{
                            String msg = jo.getString("evento");
                            Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                        }
                    }else{
                        progressDialog.cancel();
                        String msg = jo.getString("message");
                        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                    }
                }



            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en servidor para activacion de tarjeta.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }

    private class getOdometro extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //txtOdometro.setEnabled(false);
        }

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            //client.connectTimeoutMillis(60000);
            client.connectTimeoutMillis();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/get/odometer")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //txtOdometro.setEnabled(true);


            try {
                JSONObject jo = new JSONObject(s);
                if(jo.length() != 0){
                    String success = jo.getString("success");

                    if(success != "false"){
                        //progressDialog.cancel();

                        if(!(jo.isNull("message"))){
                            //new getCashCard().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, receiveUnit);
                            txtOdometro.setText(jo.getString("message"));
                            /*Toast.makeText(getBaseContext(),jo.getString("status"),Toast.LENGTH_LONG).show();
                            registerFuel.this.finish();
                            intent = new Intent(registerFuel.this, ChatActivity.class);
                            intent.putExtra("param", "1");
                            intent.putExtra("unit",receiveUnit);
                            startActivity(intent);*/
                        }else{
                            //new getCashCard().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, receiveUnit);
                            String msg = jo.getString("message");
                            Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                        }
                    }else{
                        //new getCashCard().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, receiveUnit);
                        //progressDialog.cancel();
                        String msg = jo.getString("message");
                        Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                    }
                }



            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en servidor para activacion de tarjeta.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }
    }

}
