package us.skyguardian.driverns6;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.gurtam.wiatagkit.Message;
import com.gurtam.wiatagkit.MessageSender;
import com.gurtam.wiatagkit.MessageSenderListener;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class actividades extends AppCompatActivity implements com.hzitoun.camera2SecretPictureTaker.listeners.PictureCapturingListener, ActivityCompat.OnRequestPermissionsResultCallback{

    private static String receiveParam;

    // Variables para ayuda de BD.
    private chatDbHelper cdh;
    private SQLiteDatabase db;

    // Variables de guardado de datos de URL recibida.
    private String urlCartaPorte = "";
    private String numeroviaje = "";
    private String idUnit = "";
    private String auxQuery = "";


    // Intent llamada y permiso para realizar llamadas desde el movil.
    Intent dial;
    private static final int PHONE_CALL = 102;

    // Puerto e IP para el uso de wiatag.
    String host = "193.193.165.165";
    int port = 20963;

    //Inicializa Image Bottom
    private ImageButton btnImgHoja;
    private ImageButton btnImgRoute;
    private ImageButton btnImgChat;
    private ImageButton btnImgStop;
    private ImageButton btnImgSos;

    //Variables para el permiso de localización;
    private static final int LOCATION_COARSE = 100;
    private static final int LOCATION_FINE = 101;
    private static final int READ_PHONE_STATE = 107;
    private FusedLocationProviderClient fusedLocationClient;

    String[] linkLocation = {""};

    private TextView lblNoViaje;
    private TextView lblHourSalida;
    private TextView lblHourArrive;
    private TextView lblOrigen;
    private TextView lblDestino;
    private TextView lblSenal;

    private com.hzitoun.camera2SecretPictureTaker.services.APictureCapturingService pictureService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_actividades);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("intentKey"));

        cdh = new chatDbHelper(this);
        db = cdh.getWritableDatabase();

        lblNoViaje = findViewById(R.id.xml_nolblViaje);
        lblHourSalida = findViewById(R.id.xml_lblSalidaHour);
        lblHourArrive = findViewById(R.id.xml_lblLlegada);
        lblOrigen = findViewById(R.id.xml_lblOrigen);
        lblDestino = findViewById(R.id.xml_lblDestino);
        lblSenal = findViewById(R.id.xml_lblSenal);

        //accederPermisoCall();
        //Bundle canalBundle = new Bundle();
        receiveParam = getIntent().getStringExtra("param");
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        pictureService = com.hzitoun.camera2SecretPictureTaker.services.PictureCapturingServiceImpl.getInstance(this);
        /* Modulo para la obtencion de datos de API REST*/
        try {
            JSONObject jo = new JSONObject(receiveParam);
            urlCartaPorte = jo.getString("carta_porte");
            numeroviaje = jo.getString("id_viaje");
            idUnit = jo.getString("id_unit");

            lblNoViaje.setText("No. VIAJE: " + numeroviaje);

            String auxFechasStr = "SALIDA: ";
            SpannableString str1 = new SpannableString(auxFechasStr + jo.getString("hour_exist"));
            str1.setSpan(new StyleSpan(Typeface.BOLD), 0, auxFechasStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            lblHourSalida.setText(str1);

            String auxFechaStr1 = "LLEGADA: ";
            SpannableString strA = new SpannableString(auxFechaStr1 + jo.getString("hour_arrive"));
            strA.setSpan(new StyleSpan(Typeface.BOLD), 0, auxFechaStr1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            lblHourArrive.setText(strA);

            String auxOrigenStr = "ORIGEN: ";
            SpannableString str2 = new SpannableString(auxOrigenStr + jo.getString("origen"));
            str2.setSpan(new StyleSpan(Typeface.BOLD), 0, auxOrigenStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            lblOrigen.setText(str2);

            String auxDestinoStr = "DESTINO: ";
            SpannableString str3 = new SpannableString(auxDestinoStr + jo.getString("destino"));
            str3.setSpan(new StyleSpan(Typeface.BOLD), 0, auxDestinoStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            lblDestino.setText(str3);

            String unitId = "";
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
                if ((ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE))) {
                    //mostrarExplicacionStatePhone();

                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE);
                }
            }else{
                //El permiso ya fue activado
            }

            if (telephonyManager.getDeviceId() != null) {
                unitId = telephonyManager.getDeviceId();
                Log.e("IMEI: ", unitId);
            }
            String password = "C0ntr0lCCTSky19";
            MessageSender.initWithHost(host,port,unitId,password);

            //cdh.searchNameChat(1);
            if(db != null){
                auxQuery = cdh.searchNameChat(1);
                if(auxQuery.equals("0")){
                    //System.out.println(.*)
                    cdh.addNameChat(numeroviaje);
                }else{
                    //System.out.println(.*)
                    auxQuery = cdh.searchNameChat(1);
                    if(auxQuery.equals(numeroviaje)){
                        //System.out.println(.*)
                        iniciarCronometro();
                    }else{
                        //System.out.println(.*)
                        int valida = cdh.updateNameChat(numeroviaje,1);
                        if(valida == 1){
                            //System.out.println(.*)
                            iniciarCronometro();
                        }else{
                            //System.out.println(.*)
                        }
                    }
                }
            }else{
                //System.out.println(.*)
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }



        btnImgHoja = findViewById(R.id.xml_btnImgHoja);
        btnImgRoute = findViewById(R.id.xml_btnImgRoute);
        btnImgChat = findViewById(R.id.xml_btnImgChat);
        btnImgStop = findViewById(R.id.xml_btnImgStop);
        btnImgSos = findViewById(R.id.xml_btnImgSos);

        btnImgHoja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //System.out.println(.*)
                Intent intent = new Intent (actividades.this, visorcarta.class);
                intent.putExtra("param", urlCartaPorte);
                startActivity(intent);
                Toast.makeText(getApplication(),"Obteniendo información...",Toast.LENGTH_LONG).show();
            }
        });

        btnImgRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //System.out.println(.*)
                Intent intent = new Intent (actividades.this, seeroute.class);
                intent.putExtra("paramU", idUnit);
                intent.putExtra("paramT", numeroviaje);
                startActivity(intent);
                Toast.makeText(getApplication(),"Obteniendo información...",Toast.LENGTH_LONG).show();
            }
        });

        btnImgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //System.out.println(.*)
                Intent intent = new Intent (actividades.this, ChatActivity.class);
                intent.putExtra("param", "0");
                startActivity(intent);
            }
        });

        btnImgStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //System.out.println(.*)
                Intent intent = new Intent (actividades.this, ChatActivity.class);
                intent.putExtra("param", "1");
                intent.putExtra("unit",idUnit);
                startActivity(intent);
            }
        });

        btnImgSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int validaInternet = verificarSenal();
                if(validaInternet == 1) {
                    accederPermisoLocation();
                    pictureService.startCapturing(actividades.this);
                }else{
                    if(validaInternet == 0){
                        Toast.makeText(actividades.this,"Mensaje guardado, debido a perdida de conexión de Internet.",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("keyinternet");
            if(message.isEmpty()){
                lblSenal.setVisibility(View.GONE);
            }else{
                lblSenal.setVisibility(View.VISIBLE);
                lblSenal.setText(message);
                new TextoParpadeante(getBaseContext(),lblSenal);
            }
            // Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    };

    private int verificarSenal(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return 1;
        } else {
            return 0;
        }
    }

    private void accederPermisoLocation() {

        //si la API 23 a mas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Habilitar permisos para la version de API 23 a mas

            int verificarPermisoLocationCoarse = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int verificarPermisoLocationFine = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            //Verificamos si el permiso no existe
            if (verificarPermisoLocationCoarse != PackageManager.PERMISSION_GRANTED && verificarPermisoLocationFine != PackageManager.PERMISSION_GRANTED) {
                //verifico si el usuario a rechazado el permiso anteriormente
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION) && shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Si a rechazado el permiso anteriormente muestro un mensaje
                    mostrarExplicacion();
                } else {
                    //De lo contrario carga la ventana para autorizar el permiso
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                }

            } else {
                //Si el permiso fue activado llamo al metodo de ubicacion
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
                    @Override
                    public void onSuccess(android.location.Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {

                            linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();

                            new registerEvent().execute(idUnit,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),"OPERADOR: SOS ENVIADO, ATENTA REACCIÓN, FAVOR DE MARCAR O ACTUAR DE MANERA VELOZ.");

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                    .Sos()
                                    .batteryLevel((byte)batLevel)
                                    .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }
                        else{

                            BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                            int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);

                            Message message = new Message()
                                    .time(new Date().getTime())
                                    .Sos()
                                    .batteryLevel((byte)batLevel)
                                    .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                            MessageSender.sendMessage(message,new MessageSenderListener()
                            {
                                @Override
                                protected void onSuccess() {
                                    super.onSuccess();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                @Override
                                protected void onFailure(byte errorCode) {
                                    super.onFailure(errorCode);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }

        } else {//Si la API es menor a 23 - llamo al metodo de ubicacion
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {

                        linkLocation[0] = "http://maps.google.com/maps?q=loc:"+location.getLatitude()+","+location.getLongitude();

                        new registerEvent().execute(idUnit,String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()),"OPERADOR: SOS ENVIADO, ATENTA REACCIÓN, FAVOR DE MARCAR O ACTUAR DE MANERA VELOZ.");

                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = 0;

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        }

                        Message message = new Message()
                                .time(new Date().getTime())
                                .location(new com.gurtam.wiatagkit.Location(location.getLatitude(),location.getLongitude(),location.getAltitude(),location.getSpeed(),(short) location.getBearing(),(byte)location.getAccuracy()))
                                .Sos()
                                .batteryLevel((byte)batLevel)
                                .text("Se ha presionado SOS en la ubicación: " + linkLocation[0] + " con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                    else{

                        BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);
                        int batLevel = 0;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
                        }
                        //new ChatActivity.sendMessageTelegram().execute("Se ha presionado SOS en la ubicación: " + linkLocation[0]);
                        Message message = new Message()
                                .time(new Date().getTime())
                                .Sos()
                                .batteryLevel((byte)batLevel)
                                .text("Se ha presionado SOS en la ubicación: desconocida con nivel de bateria: " + batLevel + "%");

                        MessageSender.sendMessage(message,new MessageSenderListener()
                        {
                            @Override
                            protected void onSuccess() {
                                super.onSuccess();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Mensaje SOS enviado.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                            @Override
                            protected void onFailure(byte errorCode) {
                                super.onFailure(errorCode);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplication(),"Error en el envío del mensaje.",Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    }

    private void mostrarExplicacion() {
        new AlertDialog.Builder(this)
                .setTitle("Autorización")
                .setMessage("Se requiere permiso para acceder a la ultima y actual ubicación del dispositivo.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_COARSE);
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_FINE);
                        }

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Mensaje acción cancelada
                        mensajeAccionCancelada();
                    }
                })
                .show();
    }

    public void mensajeAccionCancelada() {
        Toast.makeText(getApplicationContext(), "Haz rechazado la petición, puede suceder que la app no trabaje de manera adecuada.", Toast.LENGTH_SHORT).show();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_COARSE:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accederPermisoLocation();
                } else {
                    mostrarExplicacion();
                }
                break;
            case LOCATION_FINE:
                //Si el permiso a sido concedido abrimos la agenda de contactos
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    accederPermisoLocation();
                } else {
                    mostrarExplicacion();
                }
                break;
        }
    }

    public void onDoneCapturingAllPhotos(TreeMap<String, byte[]> picturesTaken) {
        if (picturesTaken != null && !picturesTaken.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                picturesTaken.forEach((pictureUrl, pictureData) -> {
                    //convert the byte array 'pictureData' to a bitmap (no need to read the file from the external storage) but in case you
                    //You can also use 'pictureUrl' which stores the picture's location on the device
                    final Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
                    Message message = new Message()
                            .time(new Date().getTime())
                            .image("Foto", pictureData)
                            .text("Foto recibida desde CCT por SOS.");

                    MessageSender.sendMessage(message,new MessageSenderListener()
                    {
                        @Override
                        protected void onSuccess() {
                            super.onSuccess();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplication()," Enviando imagen... ",Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                        @Override
                        protected void onFailure(byte errorCode) {
                            super.onFailure(errorCode);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplication(),"Error en el envío de imagen.",Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });
                });
            }
            //showToast("Done capturing all photos!");
            return;
        }
        //showToast("No camera detected!");
    }

    @Override
    public void onCaptureDone(String pictureUrl, byte[] pictureData) {
        if (pictureData != null && pictureUrl != null) {
            runOnUiThread(() -> {
                //convert byte array 'pictureData' to a bitmap (no need to read the file from the external storage)
                final Bitmap bitmap = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.length);
                //scale image to avoid POTENTIAL "Bitmap too large to be uploaded into a texture" when displaying into an ImageView
                final int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                final Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                //do whatever you want with the bitmap or the scaled one...
                Message message = new Message()
                        .time(new Date().getTime())
                        .image("Foto",pictureData)
                        .text("Foto recibida desde CCT por SOS. ");

                MessageSender.sendMessage(message,new MessageSenderListener()
                {
                    @Override
                    protected void onSuccess() {
                        super.onSuccess();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplication()," Enviando imagen... ",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    @Override
                    protected void onFailure(byte errorCode) {
                        super.onFailure(errorCode);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplication()," Error en envío de imagen ",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
            });
            //showToast("Picture saved to " + pictureUrl);
        }
    }

    private void showToast(final String text) {
        runOnUiThread(() ->
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show()
        );
    }

    /**
     * Inicia el servicio
     */
    private void iniciarCronometro() {
        Intent service = new Intent(this, receiveMsg.class);
        startService(service);
    }

    /**
     * Finaliza el servicio
     */
    private void pararCronometro() {
        Intent service = new Intent(this, receiveMsg.class);
        stopService(service);
    }

    public class registerEvent extends AsyncTask<String, Void, String>{

        String saveResponse = "";

        @Override
        protected String doInBackground(String... strings) {
            JSONObject paramObject = new JSONObject();
            try {
                paramObject.put("unit_id", strings[0]);
                paramObject.put("unit_x", strings[1]);
                paramObject.put("unit_y", strings[2]);
                paramObject.put("unit_txt", strings[3]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, paramObject.toString());
            Request request = new Request.Builder()
                    .url("http://192.169.213.91:9001/apps/cct/server/index.php/viaje/add/event")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    saveResponse =  response.body().string().toString();
                }else{
                    saveResponse = response.body().string().toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            //System.out.println(.*)
            return saveResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //System.out.println(.*)
            try {
                JSONObject jo = new JSONObject(result);
                String success = jo.getString("success");

                if(success != "false"){

                }else{
                    String msg = jo.getString("message");
                    Toast.makeText(getBaseContext(),msg,Toast.LENGTH_LONG).show();
                }


            } catch (JSONException e) {
                Toast.makeText(getBaseContext(),"Error en el servidor, para registro de evento.",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }
}
