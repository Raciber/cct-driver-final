package us.skyguardian.driverns6;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class myBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context,  receiveMsg.class);
        context.startService(service);
    }
}