package us.skyguardian.driverns6;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.EmptyStackException;

public class chatDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ns6.db";
    public static final String COLUMNA_ID = "_id";


    public chatDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + esquemaChatTable.chatEntry.TABLE_NAME + " ("
                + COLUMNA_ID + " integer primary key autoincrement, "
                + esquemaChatTable.chatEntry.ID_UPDATE + " TEXT DEFAULT '0',"
                + esquemaChatTable.chatEntry.ID_MSG + " TEXT DEFAULT '0',"
                + esquemaChatTable.chatEntry.ID_CHANNEL + " TEXT DEFAULT '0',"
                + esquemaChatTable.chatEntry.USER_CHANNEL + " TEXT DEFAULT '0',"
                + esquemaChatTable.chatEntry.NAME_CHAT + " TEXT DEFAULT '0')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + esquemaChatTable.chatEntry.TABLE_NAME);
        onCreate(db);
    }

    /*
    *   Metodo para agregar nombre de chat y guardarlo cuando
    *   no existe ningun dato de chat. Se usa por lo regular cuando
    *   la app se instala por primera vez.
    */
    public void addNameChat(String nombre){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(esquemaChatTable.chatEntry.NAME_CHAT, nombre);

        db.insert(esquemaChatTable.chatEntry.TABLE_NAME, null, values);
        db.close();
    }

    /*
    *  Metodo para actualizar el nombre del chat y para cuando
    * el usuario cambia de chat o es asignado a otro viaje.
    * */
    public int updateNameChat(String nombre, int id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(esquemaChatTable.chatEntry.ID_UPDATE, "0");
        values.put(esquemaChatTable.chatEntry.ID_MSG, "0");
        values.put(esquemaChatTable.chatEntry.ID_CHANNEL, "0");
        values.put(esquemaChatTable.chatEntry.USER_CHANNEL, "0");
        values.put(esquemaChatTable.chatEntry.NAME_CHAT, nombre);

        int i = db.update(
                esquemaChatTable.chatEntry.TABLE_NAME,
                values,
                COLUMNA_ID+" = ?",
                new String[]{String.valueOf( id )}
        );
        db.close();
        return i;
    }

    /*
        Metodo para obtener el valor del chat y verificar
        si existe el chat consultado en la base o ya es otro viaje.

    * */
    public String searchNameChat(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] consulta = {COLUMNA_ID, esquemaChatTable.chatEntry.NAME_CHAT};

        Cursor cursor = db.query(
                esquemaChatTable.chatEntry.TABLE_NAME,
                consulta,
                COLUMNA_ID + " = ?",
                new String[] { String.valueOf( id )},
                null,
                null,
                null,
                null
        );
        //System.out.println(.*)
        if((cursor != null) && (cursor.getCount()>0)){
            cursor.moveToFirst();
            //System.out.println(.*)
            return cursor.getString(1);

        }else{
            //System.out.println(.*)
            db.close();
            return "0";
        }

    }

    /********************************************************************************************************************************************/

    /*
     *  Metodo para la busqueda y validacion del ID Chat en BD y compararlo con el de Telegram.
     *
     * */
    public String searchIdChatUpdate(int id){

        SQLiteDatabase db = this.getReadableDatabase();
        String[] consulta = {COLUMNA_ID, esquemaChatTable.chatEntry.ID_UPDATE};

        Cursor cursor = db.query(
                esquemaChatTable.chatEntry.TABLE_NAME,
                consulta,
                COLUMNA_ID + " = ?",
                new String[] { String.valueOf( id )},
                null,
                null,
                null,
                null
        );

        if((cursor != null) && (cursor.getCount()>0)){
            cursor.moveToFirst();
            //System.out.println(.*)
            return cursor.getString(1);

        }else{
            //System.out.println(.*)
            db.close();
            return "0";
        }

    }

    /*
        Metodo para actualizar el id del chat cuando hay un nuevo mensaje.

    * */
    public int updateIdChatUpdate(String id_update, int id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(esquemaChatTable.chatEntry.ID_UPDATE, id_update);

        int i = db.update(
                esquemaChatTable.chatEntry.TABLE_NAME,
                values,
                COLUMNA_ID + " = ?",
                new String[]{String.valueOf( id )}
        );

        db.close();
        return i;
    }

    /********************************************************************************************************************************************/

    /*
     *  Metodo para la busqueda y validacion del ID Chat en BD y compararlo con el de Telegram.
     *
     * */
    public String searchIdChat(int id){

        SQLiteDatabase db = this.getReadableDatabase();
        String[] consulta = {COLUMNA_ID, esquemaChatTable.chatEntry.ID_CHANNEL};

        Cursor cursor = db.query(
                esquemaChatTable.chatEntry.TABLE_NAME,
                consulta,
                COLUMNA_ID + " = ?",
                new String[] { String.valueOf( id )},
                null,
                null,
                null,
                null
        );

        if((cursor != null) && (cursor.getCount()>0)){
            cursor.moveToFirst();
            //System.out.println(.*)
            return cursor.getString(1);

        }else{
            //System.out.println(.*)
            db.close();
            return "0";
        }

    }

    /*
        Metodo para actualizar el id del chat cuando hay un nuevo mensaje.

    * */
    public int updateIdChat(String id_chat, int id){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(esquemaChatTable.chatEntry.ID_CHANNEL, id_chat);

        int i = db.update(
                esquemaChatTable.chatEntry.TABLE_NAME,
                values,
                COLUMNA_ID + " = ?",
                new String[]{String.valueOf( id )}
        );

        db.close();
        return i;
    }
}
